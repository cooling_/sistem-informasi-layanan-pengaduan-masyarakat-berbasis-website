<?php

//DATABASE CONNECTION
include 'koneksi.php';

$pass = md5($_POST['password']);
$username = mysqli_escape_string($koneksi, $_POST['username']);
$password = mysqli_escape_string($koneksi, $pass);
$level = mysqli_escape_string($koneksi, $_POST['level']);

//USERNAME CHECK
$cek_user = mysqli_query($koneksi, "SELECT * FROM user WHERE username = '$username' and level = '$level' ");
$user_valid = mysqli_fetch_array($cek_user);

//USERNAME VALIDATION
if ($user_valid) {
	//USERNAME REGISTERED
	//PASSWORD CHECK
	if ($password == $user_valid['password']) {
		//PASSWORD CHECKLIST
		//SESSION
		session_start();
		$_SESSION['username'] = $user_valid['username'];
		$_SESSION['full_name'] = $user_valid['full_name'];
		$_SESSION['level'] = $user_valid['level'];

		//TEST USER LEVEL
		if ($level == 'Admin') {
			header('location:../Dashboard_admin/admin.php');
		} elseif ($level == 'Petugas') {
			header('location:../Dashboard_petugas/petugas.php');
		} elseif ($level == 'Masyarakat') {
			header('location:../Dashboard_masyarakat/masyarakat.php');
		}

	} else {
		echo "<script>alert('Password tidak terdaftar!');
		document.location = 'signup.php'</script>";
	}

} else {
	echo "<script>alert('Username tidak terdaftar!');
	document.location = 'signup.php'</script>";
}

?>