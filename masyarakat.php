<?php
session_start();
if (empty($_SESSION['username']) or empty($_SESSION['level'])) {
		echo "<script>alert('Sorry, You have to login first');
		document.location = '../signin-signup/signup.php'</script>";
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<author name = "Muhammad Fatkhur Rahman">
	<link rel="stylesheet" type="text/css" href="../CSS/dashboard.css">
	<title>Dashboard-masyarakat</title>
</head>
<body>

	<!-- SIDE-BAR -->
	<section id="sideBar">
		<a href="#" class="brand"><img src="logo.jpg" class="logo">Walters</a>
		<ul class="side-menu">
			<li>
				<a href="masyarakat.php" class="active"><i><img src="grid.svg"></i>Dashboard</a>
			</li>
			<li class="divider" data-text="user">User</li>
			<li>
				<a href="#"><i><img src="user-plus.svg"></i>Society<img src="chevron-down-black.svg" class="Icon"></a>
				<ul class="side-dropdown">
					<li><a href="profil.php">Profil</a></li>
					<li><a href="?url=ganti-password">Change Password</a></li>
				</ul>
			</li>
			<li class="divider" data-text="data">Data</li>
			<li><a href="?url=form-pengaduan"><i><img src="file-text.svg"></i>Form Complaint</a></li>
			<li><a href="?url=generate-report"><i><img src="file.svg"></i>Generate Report</a></li>
			<li class="divider" data-text="site">Site</li>
			<li><a href="../signin-signup/logout.php"><i><img src="log-out.svg"></i>Log Out</a></li>
		</ul>
	</section>
	<!-- SIDE-BAR END -->

	<!-- NAVIGATION BAR -->
	<section id="content">
		<nav>
			<i><img src="menu.svg" class="toggle-sidebar"></i>
			<form action="#">
				<div class="form-group">
					<input type="text" name="" placeholder="Search">
					<i><img src="search.svg" class="icon"></i>
				</div>
			</form>
			<a href="#" class="nav-link">
				<i><img src="bell.svg" class="icon"></i>
				<span class="badge"></span>
			</a>
			<a href="#" class="nav-link">
				<i><img src="message-square.svg" class="icon"></i>
				<span class="badge"></span>
			</a>
			<span class="divider"></span>
			<div class="profile">
				<img src="pnguser.png" class="user">
				<ul class="profile-link">
					<li><a href="profil.php"><i><img src="user-black.svg"></i>Profile</a></li>
					<li><a href="../signin-signup/logout.php"><i><img src="log-out.svg"></i>Logout</a></li>
				</ul>
			</div>
		</nav>
		<!-- NAVIGATIO BAR END -->

		<!-- MAIN -->
		<main>
			<div class="head">
				<h4><?php include '../halaman/halaman.php'?></h4>
			</div>
		</main>
	</section>
<script src="../JS/dashboard.js"></script>
</body>
</html>