<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="signup.css">
	<title>sign</title>
</head>
<body>
	<div class="container">
		<div class="signin-signup">
			<form action="cek_login.php" method="POST" class="sign-in-form">
				<h2 class="title">Sign In</h2>
				<div class="input-field">
					<label for="user"></label>
					<i><img src="user.svg"></i>
					<input type="text" placeholder="Username" name="username" required>
				</div>
				<div class="input-field">
					<label for="pass"></label>
					<i><img src="lock.svg"></i>
					<input type="password" placeholder="Password" name="password" required>
				</div>
				<select name="level">
					<option value="Admin">Admin</option>
					<option value="Petugas">Petugas</option>
					<option value="Masyarakat">Masyarakat</option>
				</select>
				<input type="submit" name="login" value="Login" class="btn">
				<p class="social-text">We Can Do It</p>
			</form>
			
			<form action="cek_register.php" method="POST" class="sign-up-form">
				<h2 class="title">Sign Up</h2>
				<div class="input-field">
					<i><img src="edit-2.svg"></i>
					<input type="text" placeholder="Full Name" name="full_name" required value="">
				</div>
				<div class="input-field">
					<i><img src="edit-2.svg"></i>
					<input type="text" placeholder="nik" name="nik" required value="">
				</div>
				<div class="input-field">
					<i><img src="user.svg"></i>
					<input type="text" placeholder="Username" name="username" required value="">
				</div>
				<div class="input-field">
					<i><img src="mail.svg"></i>
					<input type="text" placeholder="Email" name="email" required value="">
				</div>
				<div class="input-field">
					<i><img src="lock.svg"></i>
					<input type="password" placeholder="Password" name="password" required value="">
				</div>
				<select name="level">
					<option value="Admin">Admin</option>
					<option value="Petugas">Petugas</option>
					<option value="Masyarakat">Masyarakat</option>
				</select>
				<input type="submit" name="submit" value="Sign Up" class="btn">
				<p class="social-text">We Change Everything</p>
			</form>
		</div>
		<div class="panels-container">
			<div class="panel left-panel">
				<div class="content">
					<h3>People Society</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					<button class="btn" id="sign-in-btn" name="submit">Sign In</button>
				</div>
				<img src="upgrade.svg" alt="" class="image">
			</div>
			<div class="panel right-panel">
				<div class="content">
					<h3>Join Us</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					<button class="btn" id="sign-up-btn" name="submit">Sign Up</button>
				</div>
				<img src="team.svg" alt="" class="image">
			</div>
		</div>
	</div>
	<script type="text/javascript" src="signup.js"></script>
</body>
</html>