<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<author name = "Muhammad Fatkhur Rahman">
	<link rel="stylesheet" type="text/css" href="../CSS/dashboard.css">
	<title>Dashboard-masyarakat</title>
</head>
<body>
	<!-- NAVIGATION BAR -->
	<section>
		<!-- NAVIGATIO BAR END -->

		<!-- MAIN -->
		<main>
			<h1 class="title">Dashboard</h1>
			<ul class="breadcrumbs">
				<li><a href="masyarakat.php">Home</a></li>
				<li class="divider">/</li>
				<li><a href="form_pengaduan.php" class="active">Dashboard</a></li>
			</ul>
			<div class="info-data">
				<div class="card">
					<div>
						<form action = "proses-pengaduan.php" method = "post" enctype = "multipart/form-data">
							<div class="card">
								<label>Complaint Date</label>
								<input type="text" name="tgl" class="form-control" disabled value="<?= date('d/m/Y'); ?>">
							</div>
							<div class="card">
								<label>NIK</label>
								<input type="number" name="nik" class="form-control" readonly value="<?php echo $_SESSION['nik']; ?>">
							</div>
							<div class="card">
								<label>Name</label>
								<input type="text" name="full_name" class="form-control" readonly value="<?php echo $_SESSION['full_name']; ?>">
							</div>
							<div class="card">
								<label>Report</label>
								<textarea name="laporan" class="form-control"></textarea>
							</div>
							<div class="card">
								<label>Upload photo</label>
								<input type="file" name="foto" class="form-control" accept=".jpg, .jpeg, .png, .gif"><font>*tipe yang bisa di upload adalah : .jpg, .jpeg, .png, .gif</font>
							</div>
							<div class="card">
								<button type="submit" value="submit"class="btn">Save</button>
								<button type="reset" value="reset" class="btn">Empty</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</main>
	</section>
	<script src="../JS/dashboard.js"></script>
</body>
</html>