<?php
require 'functions.php';

$id = $_GET["id"];

$pend = query("SELECT * FROM admin WHERE id = $id")[0];

if (isset($_POST["submit"])) {

    if( ubah($_POST) > 0) {
        echo "
        <script>
            alert('Data Berhasil Diubah');
            document.location.href = 'incoming_complaint.php';
            </script>
            ";
        } else { 
            echo "
            <script>
            alert('Data Gagal Diubah');
            document.location.href = 'Edit.php';
            </script>
            ";
    }
}
?>

<html>
    <body>
    <head>
    <link rel="stylesheet" type="text/css" href="" />
        <title>Daftar Pengaduan</title>
    </head>
        <div class="container">
        <h1 class="judul"> Ubah Data </h1>

        <form class="form" action="" method="post">
            <input type="hidden" name="id" value="<?= $pend["id"]; ?>">
            <ul class="label">
                <li>
                    <label for="nik">NIK</label>
                    <input type="text" name="nik" id="nik" required value="<?= $pend["nik"]; ?>">
                </li>
                <li>
                    <label for="tanggal">Tanggal Pengaduan</label>
                    <input type="text" name="tanggal" id="tanggal" required value="<?= $pend["tgl_pengaduan"]; ?>">
                </li>
                <li>
                    <label for="full_name">Nama Lengkap</label>
                    <input type="text" name="full_name" id="nama" value="<?= $pend["full_name"]; ?>">
                </li>
                <li>
                    <label for="isi_laporan">Isi Laporan</label>
                    <input type="text" name="isi_laporan" id="isi_laporan" value="<?= $pend["laporan"]; ?>">
                </li>
                <li>
                    <label for="foto">Foto</label>
                    <input type="text" name="foto" id="foto" value="<?= $pend["foto_laporan"]; ?>">
                </li>
                <li>
                    <label for="keterangan">Keterangan</label>
                    <input type="text" name="keterangan" id="keterangan" value="<?= $pend["keterangan"]; ?>">
                </li>
                <li>
                    <button type="submit" name="submit"> Ubah Data </button>
                </li>
            </ul>
        </form>
        </div>
    </body>
</html>
