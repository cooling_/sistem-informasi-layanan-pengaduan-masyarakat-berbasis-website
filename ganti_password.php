<?php
session_start();
if (empty($_SESSION['username']) or empty($_SESSION['level'])) {
		echo "<script>alert('Sorry, You have to login first');
		document.location = '../signin-signup/signup.php'</script>";
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<author name = "Muhammad Fatkhur Rahman">
	<link rel="stylesheet" type="text/css" href="ganti_password.css">
	<title>Change Password</title>
</head>
<body>

	<!-- SIDE-BAR -->
	<section id="sideBar">
		<a href="admin.php" class="brand"><img src="logo.jpg" class="logo">Walters</a>
		<ul class="side-menu">
			<li><a href="admin.php" class="active"><i><img src="grid-white.svg"></i>Dashboard</a></li>
			<li class="divider" data-text="user">User</li>
			<li>
				<a href="#"><i><img src="users.svg"></i>Admin<img src="chevron-down-black.svg" class="Icon"></a>
				<ul class="side-dropdown">
					<li><a href="#">Profil</a></li>
					<li><a href="ganti_password.php">Change Password</a></li>
					<li><a href="#">Add Users</a></li>
				</ul>
			<li>
			<li class="divider" data-text="data">Data</li>
			<li>
				<a href="#"><i><img src="database.svg"></i>Master Data<img src="chevron-down-black.svg" class="Icon"></a>
				<ul class="side-dropdown">
					<li><a href="#">Incoming Complaint</a></li>
					<li><a href="#">Process Complaint</a></li>
					<li><a href="#">Response Complaint</a></li>
					<li><a href="#">Done Complaint</a></li>
				</ul>
			</li>
			<li><a href="#"><i><img src="file.svg"></i>Generate Report</a></li>
			<li class="divider" data-text="site">Site</li>
			<li><a href="../signin-signup/logout.php"><i><img src="log-out.svg"></i>Log Out</a></li>
		</ul>
	</section>
	<!-- SIDE-BAR END -->

	<!-- NAVIGATION BAR -->
	<section id="content">
		<nav>
			<i><img src="menu.svg" class="toggle-sidebar"></i>
			<form action="#">
				<div class="form-group">
					<input type="text" name="" placeholder="Search">
					<i><img src="search.svg" class="icon"></i>
				</div>
			</form>
			<a href="#" class="nav-link">
				<i><img src="bell.svg" class="icon"></i>
				<span class="badge"></span>
			</a>
			<a href="#" class="nav-link">
				<i><img src="message-square.svg" class="icon"></i>
				<span class="badge"></span>
			</a>
			<span class="divider"></span>
			<div class="profile">
				<img src="pnguser.png" class="user">
				<ul class="profile-link">
					<li><a href="#"><i><img src="user-black.svg"></i>Profile</a></li>
					<li><a href="../signin-signup/logout.php"><i><img src="log-out.svg"></i>Logout</a></li>
				</ul>
			</div>
		</nav>
		<!-- NAVIGATIO BAR END -->

		<!-- MAIN -->
		<main>
			<h1 class="title">Change Password</h1>
			<ul class="breadcrumbs">
				<li><a href="admin.php">Home</a></li>
				<li class="divider">/</li>
				<li><a href="ganti_password.php" class="active">Change Password</a></li>
			</ul>
			<div class="info-data">
				<div class="card">
					<div>
						<form action = "change_passwordDb.php" method = "post">
							<input type="hidden" name="username" value="<?= $_SESSION['username'] ?>">
							<div class="card">
								<label>Current Password</label>
								<input type="password" name="current_password" class="form-control" required>
							</div>
							<div class="card">
								<label>New Password</label>
								<input type="password" name="new_password" class="form-control" required>
							</div>
							<div class="card">
								<label>Repeat Password</label>
								<input type="password" name="repeat_password" class="form-control" required>
							</div>
							<div class="card">
								<button type="submit" class="btn">Change Password</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</main>
	</section>
	<script src="admin.js"></script>
</body>
</html>

