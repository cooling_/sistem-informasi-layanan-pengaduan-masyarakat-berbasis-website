window.addEventListener("scroll", function() {
	var navwrapper = document.querySelector("navwrapper");
	navwrapper.classList.toggle("sticky", window.scrollY > 0);
}