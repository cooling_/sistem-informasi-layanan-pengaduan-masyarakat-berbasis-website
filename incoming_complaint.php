<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<author name = "Muhammad Fatkhur Rahman">
		<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="../CSS/dashboard.css">
		

	<title>Incoming Complaint</title>
</head>
<body>
	<!-- NAVIGATION BAR -->
	<section>
		<!-- MAIN -->
		<main>
			<h1 class="title">Dashboard</h1>
			<ul class="breadcrumbs">
				<li><a href="#">Home</a></li>
				<li class="divider">/</li>
				<li><a href="#" class="active">Dashboard</a></li>
			</ul>
			<div class="info-data">
				<div class="card">
					<div class="cont">
						<table border="1" cellpadding="10" cellspacing="0" class="table">
							<tr>
								<th>ID</th>
								<th>Tanggal Pengaduan</th>
								<th>NIK</th>
								<th>Nama Lengkap</th>
								<th>Isi Laporan</th>
								<th>Foto</th>
								<th>Keterangan</th>
								<th>Opsi</th>
							</tr>
							<?php 
							include "../Koneksi_database/koneksi.php";
							$query_mysqli  = mysqli_query($koneksi, "SELECT * FROM admin")or die(mysqli_error());
							$nomor = 1;
							while($data = mysqli_fetch_array($query_mysqli )){
								?>
							<tr>
								<td><?php echo $nomor++; ?></td>
								<td><?php echo $data['tgl_pengaduan']; ?></td>
								<td><?php echo $data['nik']; ?></td>
								<td><?php echo $data['full_name']; ?></td>
								<td><?php echo $data['laporan']; ?></td>
								<td><?php echo $data['foto_laporan']; ?></td>
								<td><?php echo $data['keterangan']; ?></td>
								<td>
									<a class="edit" href="edit.php?id=<?php echo $data['id']; ?>">Edit</a> |
									<a class="hapus" href="hapus.php?id=<?php echo $data['id']; ?>">Hapus</a>
								</td>
							</tr>
							<?php } ?>
						</table>
					</div>
				</div>
			</div>
		</main>
	</section>
	<script src="../JS/dashboard.js"></script>
	<!-- JQUERY AJAX -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
</body>
</html>