<?php

session_start();
unset($_SESSION['username']);
unset($_SESSION['password']);
unset($_SESSION['full_name']);
unset($_SESSION['level']);

session_destroy();
echo "<script>alert('You have logout the Admin page');
document.location = '../signin-signup/signup.php'</script>";