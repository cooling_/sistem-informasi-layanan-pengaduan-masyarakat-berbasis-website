<?php

include '..Koneksi_database/koneksi.php';
$_SESSION['id'] = 1;
$sessionId = $_SESSION['id'];
$user = mysql_fetch_assoc(mysqli_query($koneksi, "SELECT * FROM user WHERE id = $sessionId"));

// session_start();
// if (empty($_SESSION['username']) or empty($_SESSION['level'])) {
// 		echo "<script>alert('Sorry, You have to login first');
// 		document.location = '../signin-signup/signup.php'</script>";
// }

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<author name = "Muhammad Fatkhur Rahman">
	<link rel="stylesheet" type="text/css" href="../CSS/dashboard.css">
	<title>Profil</title>
</head>
<body>

	<!-- SIDE-BAR -->
	<section id="sideBar">
		<a href="admin.php" class="brand"><img src="logo.jpg" class="logo">Walters</a>
		<ul class="side-menu">
			<li><a href="admin.php" class="active"><i><img src="grid-white.svg"></i>Dashboard</a></li>
			<li class="divider" data-text="user">User</li>
			<li>
				<a href="#"><i><img src="users.svg"></i>Admin<img src="chevron-down-black.svg" class="Icon"></a>
				<ul class="side-dropdown">
					<li><a href="#">Profil</a></li>
					<li><a href="ganti_password.php">Change Password</a></li>
					<li><a href="#">Add Users</a></li>
				</ul>
			<li>
			<li class="divider" data-text="data">Data</li>
			<li>
				<a href="#"><i><img src="database.svg"></i>Master Data<img src="chevron-down-black.svg" class="Icon"></a>
				<ul class="side-dropdown">
					<li><a href="#">Incoming Complaint</a></li>
					<li><a href="#">Process Complaint</a></li>
					<li><a href="#">Response Complaint</a></li>
					<li><a href="#">Done Complaint</a></li>
				</ul>
			</li>
			<li><a href="#"><i><img src="file.svg"></i>Generate Report</a></li>
			<li class="divider" data-text="site">Site</li>
			<li><a href="../signin-signup/logout.php"><i><img src="log-out.svg"></i>Log Out</a></li>
		</ul>
	</section>
	<!-- SIDE-BAR END -->

	<!-- NAVIGATION BAR -->
	<section id="content">
		<nav>
			<i><img src="menu.svg" class="toggle-sidebar"></i>
			<form action="#">
				<div class="form-group">
					<input type="text" name="" placeholder="Search">
					<i><img src="search.svg" class="icon"></i>
				</div>
			</form>
			<a href="#" class="nav-link">
				<i><img src="bell.svg" class="icon"></i>
				<span class="badge"></span>
			</a>
			<a href="#" class="nav-link">
				<i><img src="message-square.svg" class="icon"></i>
				<span class="badge"></span>
			</a>
			<span class="divider"></span>
			<div class="profile">
				<img src="pnguser.png" class="user">
				<ul class="profile-link">
					<li><a href="profil.php"><i><img src="user-black.svg"></i>Profile</a></li>
					<li><a href="../signin-signup/logout.php"><i><img src="log-out.svg"></i>Logout</a></li>
				</ul>
			</div>
		</nav>
		<!-- NAVIGATIO BAR END -->

		<!-- MAIN -->
		<main>
			<h1 class="title">Dashboard</h1>
			<ul class="breadcrumbs">
				<li><a href="#">Home</a></li>
				<li class="divider">/</li>
				<!-- <li><a href="#" class="active">Dashboard</a></li> -->
			</ul>
			<div class="user-image">
				<div class="foto">
					<img src="uploads/<?php echo $foto; ?>">
				</div>
				<div class="button-block" id="editPic">
					<a href="editPic.php" class="button EditPicButton">Edit Profile Picture</a>	
				</div>
				<div class="help-block">
					<a href="../Landing%Page/Landing%Page.html">Need Help</a>
				</div>
				
			</div>
		</main>
	</section>
	<script src="../JS/dashboard.js"></script>
</body>
</html>